using System;
using Common;

namespace BrightApp
{
	public class BrightGreetingService : IGreetingService
	{
		#region IGreetingService implementation

		public string GetGreeting()
		{
			return "Hello from the bright app!";
		}

		#endregion
	}
}
