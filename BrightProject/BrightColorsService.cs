using System;
using Common;
using MonoTouch.UIKit;

namespace BrightApp
{
	public class BrightColorsService : IColorsService
	{
		public BrightColorsService()
		{
		}

		#region IColorsService implementation

		public MonoTouch.UIKit.UIColor GetBackgroundColor()
		{
			return UIColor.Blue;
		}

		public MonoTouch.UIKit.UIColor GetForegroundColor()
		{
			return UIColor.Red;
		}

		#endregion
	}
}
