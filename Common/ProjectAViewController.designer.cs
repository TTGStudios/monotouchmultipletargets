// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace Common
{
	[Register ("ProjectAViewController")]
	partial class ProjectAViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView subview { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel label { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel greetingLabel { get; set; }

		[Action ("greetingButtonPressed:")]
		partial void greetingButtonPressed (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (subview != null) {
				subview.Dispose ();
				subview = null;
			}

			if (label != null) {
				label.Dispose ();
				label = null;
			}

			if (greetingLabel != null) {
				greetingLabel.Dispose ();
				greetingLabel = null;
			}
		}
	}
}
