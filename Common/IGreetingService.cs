using System;

namespace Common
{
	public interface IGreetingService
	{
		string GetGreeting();
	}
}
