using System;
using MonoTouch.UIKit;

namespace Common
{
	public interface IColorsService
	{
		UIColor GetBackgroundColor();
		UIColor GetForegroundColor();
	}
}
