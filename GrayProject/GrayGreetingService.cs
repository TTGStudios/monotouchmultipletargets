using System;
using Common;

namespace GrayApp
{
	public class GrayGreetingService : IGreetingService
	{
		#region IGreetingService implementation

		public string GetGreeting()
		{
			return "Hello from the gray app!";
		}

		#endregion
	}
}
