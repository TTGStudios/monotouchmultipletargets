using System;
using Common;
using MonoTouch.UIKit;

namespace GrayApp
{
	public class GrayColorsService : IColorsService
	{
		public GrayColorsService()
		{
		}

		#region IColorsService implementation

		public MonoTouch.UIKit.UIColor GetBackgroundColor()
		{
			return UIColor.DarkGray;
		}

		public MonoTouch.UIKit.UIColor GetForegroundColor()
		{
			return UIColor.LightGray;
		}

		#endregion
	}
}
